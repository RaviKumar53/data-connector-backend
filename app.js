/**
 * Main application file
 */

const host = 'node-api.xxx.net';

const RedashClient = require('redash-client');



const redash = new RedashClient({
  endPoint: 'http://ec2-65-0-83-141.ap-south-1.compute.amazonaws.com/',
  apiToken: 'cIpaxZi9GDKYkX0gSrhJ25obT4vn7Ky6OTaEwXtr',
});
redash
  .queryAndWaitResult({
    query: 'select * from salary',
    data_source_id: 1,
  })
  .then(resp => {
    console.log(JSON.stringify(resp.query_result),'redash working');
  }).catch(err=>console.log('could not get data'));


var env = process.env.NODE_ENV;
var express = require('express');
const bodyParser = require('body-parser');

global.CONF_log = require('./config/environment');
global.logLevel = global.CONF_log.logLevel;
var PropertiesReader = require('properties-reader');
var envurl = PropertiesReader('./properties/envurl.properties');
var config = require('./config/environment');
//var dbUtil = require('./api/utilities/dbutils');

var http = require('http');
var log = require('loglevel').getLogger('app');

// Setup server
var app = express();

var server = http.createServer(app);
require('./config/express').default(app);
require('./routes').default(app);
var path = require('path');
app.use(express.static(path.join(__dirname, 'public')));

// Start server
function startServer() {
  app.angularFullstack = server.listen(config.port, config.ip, function() {
	console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
    try {
      //
    } catch (e) {
      console.log('Not able to connect to server');
    }
  });
}

app.set('json replacer', function(key, value) {
  // undefined values are set to `null`
  if (typeof value === 'undefined') {
    return null;
  }
  return value;
});

startServer();

app.get('/ping', function pong(req, res) {
  console.log(null, 'app.health', null, 'health is called.');
  res.send({ msg: 'pong' });
});

// Expose app
exports = module.exports = app;
