exports.isStrValid = function(teststr){
  return /^[0-9]{15}$/.test(teststr);
}

exports.isVinValid = function(vin){
  if(!vin || typeof vin !== 'string') return false;
  if(!(/^[A-Z0-9]{17}$/.test(vin))) return false;
  return !(vin.indexOf('I') > -1 || vin.indexOf('O') > -1 || vin.indexOf('Q') > -1);
}


