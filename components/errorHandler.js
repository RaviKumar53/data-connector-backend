var PropertiesReader = require('properties-reader');
var errorcodes = PropertiesReader('./properties/error_codes.properties');

 exports.error = function(res, errcode,errmsg,field,title){
    let errorMessage = {};
    let errors = [];
    let e = {};
    e.code = errorcodes.get(errcode) || errcode;
    e.message = errorcodes.get(errmsg) || errmsg;
    e.field = field || '';
    e.title = title || '';
    errors.push(e);
    errorMessage.errors = errors;

    console.log('Error: ' + ((typeof errorMessage === 'object') ? JSON.stringify(errorMessage) : errorMessage));
    // nodelogger.error("Error: " + ((typeof error === "object") ? JSON.stringify(err) : err));
    res.status(400).json(errorMessage);
    res.end();
  };
  
  exports.errorResponse = function(ecd, msgcd, efield, title) {
  let errorMessage = {};
  let errors = [];
  let e = {};
  e.code = ecd || 200;
  e.message = msgcd || 'NA';
  e.field = efield || 'NA';
  e.title = title || '';

  errors.push(e);
  // errorMessage.msg = msgcd;
  errorMessage.errors = errors;

  return errorMessage;
};

  exports.error_401 = function(res, errcode,errmsg,field,title){
    let errorMessage = {};
    let errors = [];
    let e = {};
    e.code = errorcodes.get(errcode) || errcode;
    e.message = errorcodes.get(errmsg) || errmsg;
    e.field = field || '';
    e.title = title || '';
    errors.push(e);
    errorMessage.errors = errors;

    console.log('Error: ' + ((typeof errorMessage === 'object') ? JSON.stringify(errorMessage) : errorMessage));
    // nodelogger.error("Error: " + ((typeof error === "object") ? JSON.stringify(err) : err));
    res.status(401).json(errorMessage);
    res.end();
  };


  exports.success = function(res,data,msg,title){
    let message = {
        errors: [],
        result: {
          message: msg,
          title: title,
          data: data
        }
      }
    if(data === null || data === undefined){
        res.status(200).json({})
    }else{
        res.status(200).json(message);
    }
    res.end();
  };


exports.serverError = function(res, ex){
    let errorMessage = {};
    let errors = [];
    let e = {};
    e.code = 500;
    e.message = 'Server Error' || '';
    e.field = '';
    e.title = '';
    errors.push(e);
    errorMessage.errors = errors;

    console.log('Error: ' + ((typeof errorMessage === 'object') ? JSON.stringify(errorMessage) : errorMessage));
     console.log('Exception: ', ex.stack || ex.toString());
     // nodelogger.error(`Exception: `, ex.stack || ex.toString());
     res.status(500).json(errorMessage);
   };
  
  