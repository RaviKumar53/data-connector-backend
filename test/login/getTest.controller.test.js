const chai = require('chai');
const app = require('../../app');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

var expect = require('chai').expect;

var request = require('supertest');


describe('getTest.controller', function() {
 // test cases 
 this.enableTimeouts(false);
 before(function(done) {
   console.log('Inside first before !!!');
   setTimeout(() => {
     console.log('Inside first ');
     done();
   }, 15000);
 });

 before(function(done) {
   request(app).get('/api/login/getLoginDetails').set('Accept', 'application/json').send()
   .expect(200).end(function(err, res){
     if (err)done(err);
     console.log('first test case is :::::');
     done();
   });
 });

 it('should return a response with HTTP code 200 and the response text should an array of json objects', function(done) {
   request(app).get('/api/login/getLoginDetails')
     .expect(200).end((err, res) => {
       if (err) done(err);
       expect(res.status).to.equal(200);
       expect(res.body.result.data).to.be.an('object');
       done();
     });
 });

});
