/*
 * Main application routes
 */
var express = require('express');
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
var basicAuth = require('basic-auth');
var config = require('./config/environment');
var envurl = config.envProperties;
var path = require('path');
var basepath = envurl.get('DOMAIN.basepath');
const cors = require('cors')
const session = require('express-session')




exports.default = function (app) {
  var auth = function (req, res, next) {
    function unauthorized(res) {
      res.set('WWW-Authenticate', 'Basic realm=Authorization Required');
      return res.send(401);
    };

    var user = basicAuth(req);

    if (!user || !user.name || !user.pass) {
      return unauthorized(res);
    };

  };
  app.disable('x-powered-by');
  const allowedMethods = ['GET', 'POST', 'OPTIONS'];
  app.use(
    function onrequest(req, res, next) {
      if (!allowedMethods.includes(req.method)) {
        res.status(412);
        res.send('Method Not Allowed');
        res.end();
      } else {
        next();
      }
    });

  app.use(cors({
    origin: ["http://localhost:3000"],
    methods: ["GET", "POST"],
    credentials: true,
  }))
  app.use(express.json())
  app.use(cookieParser());
  app.use(bodyParser.urlencoded({ extended: true }))

  app.use(session({
    key: "userId",
    secret: "subscribe",
    resave: false,
    saveUninitialized: false,
    cookie: {
      expires: 60 * 60 * 24,
    }
  }))

  app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,POST,PUT');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, tkn');

    (req.method == 'OPTIONS') ? res.sendStatus(204) : next();
  });

  // Insert routes below

  app.use('/api/login', require('./api/login'));
  // all undefined routes should point return 404 and nothing else
};
