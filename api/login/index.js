const express = require('express');
const router = express.Router();
const mysql = require('mysql')
const bcrypt = require('bcrypt');
const saltRounds = 10


const session=require('express-session')


const db = mysql.createConnection({
  user: 'root',
  host: 'localhost',
  password: '1234',
  database: 'ebdb',
  port: 3306
}, () => console.log('connection created'))

const getLoginController = require('./getLogin.controller.js');
const login = require('./login')

router.get('/getLoginDetails', getLoginController.getLoginController);
// // router.get('/',login.login)

// router.post('/', (req, res, next) => {
//   res.json({ name: 'ravi' })
// })

router.post('/register', (req, res) => {
  const email = req.body.email;
  const role = req.body.role;
  const password = req.body.password;
  console.log(email, 'email')
  bcrypt.hash(password, saltRounds, (err, hash) => {
    if (err) {
      console.log(err)
    }
    db.query(
      "insert into ebdb.login (email,password,role) values(?,?,?)", [email, hash, role],
      (err, result) => {
        if (err) console.log(err)
        else res.send({ data: 'data successfully registered' })
      }
    )
  })
})

router.post('/', (req, res) => {
  const email = req.body.email;
  const password = req.body.password;
  db.query("select * from ebdb.login where email=?", email,
    (err, result) => {
      if (err) {
        console.log(err, 'error')
        res.send({ err: err })
      }
      if (result.length > 0) {     //User exist in this
        console.log(result, 'result')
        bcrypt.compare(password, result[0].password, (err, response) => {
          if (response) {
            res.send(result)
          }
          else {
            res.send({ message: "wrong email n password" })
          }
        })
      }
      else {          //User doesn't exist
        res.send({ message: "User doesn't exist" })
      }
    })
})

module.exports = router;
