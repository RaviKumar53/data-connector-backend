var jwt = require('jsonwebtoken');
const CONF = require('../../config/environment');
const JWT = CONF.JWT;
const async = require('async');
const dbUtils = require('../../utilities/dbutils');

async function generateTokens(requestData, requestor, callback) {
    // Generate access_token and refresh_token
    var validRequest = ( ('user_id' in  requestData.jwtObj));
    if(!('jwtObj' in requestData) || !validRequest ){
        callback(new Error('Not able to process the request. Please try again later.'), null);
    } else{
        async.waterfall([
            function(cb) {
                var jwtObj = requestData.jwtObj;
                var access_token = jwt.sign(jwtObj, JWT.SC_KEY, {
                    algorithm: JWT.ALG,
                    expiresIn: JWT.KEY_LFTM,
                });
                /*var refreshObj = {user_id: jwtObj.user_id, role_id: jwtObj.role_id};
                var refresh_token;
                if(requestor === 'LOGIN') {
                    // default_contract_id: jwtObj.default_contract_id
                    refresh_token = jwt.sign(refreshObj, JWT.REFRESH_SECRET, {
                        algorithm: JWT.ALG,
                        expiresIn: JWT.REFRESH_LIFE,
                    });
                } else {
                    refresh_token = null;
                }*/               
                requestData.tkn = access_token;
                //requestData.refresh_token = refresh_token;
                cb(null, requestData);        
            }, function(result, cb) {
                cb(null, result);
            }
        ], function(error, response) {
            if(error) {
                callback(error, null);
            } else {
                callback(null, response);
            }
        });
    }
}


module.exports = {
    generateTokens
}

