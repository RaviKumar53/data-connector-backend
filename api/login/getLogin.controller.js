const async = require('async');
var parser = require('body-parser');
var PropertiesReader = require('properties-reader');
var errorcodes = PropertiesReader('./properties/error_codes.properties');
var errHandler = require('../../components/errorHandler');
const conf = require('../../config/environment');
var jwt = require('jsonwebtoken');

const express = require('express');
const app = express();
const Logger = require('../../services/logger_service.js');
const logger = new Logger('app');
const bodyParser = require('body-parser');

var dbUtil = require('../../utilities/dbutils');
var commonService = require('./commonService');

app.use(
	bodyParser.urlencoded({
		 extended: false
	})
 );

app.use(bodyParser.json());

exports.getLoginController = function(req, res) {
  let error = {};
  let requestData = req.body;

  //console.log("Request recieved at /getLoginController ::::: ");
  logger.info('Request recieved at /getLoginController ::::: ');
	let errorMessage = {}
	let errors = [];
    try {
      async.waterfall([
      ], function(e, result) {
        if (e) {
			  res.status(400).send(e);
        } else {
          getUSerDetails(requestData, res);
            /*let ResponseJson = {}
			      ResponseJson.USER_DETAILS = "USER_DETAILS";
            res.status(200).send({
            errors: [],
            result: {
              message: "Test login fetched successfully !",
              title: '',
              data: ResponseJson
            }
          });*/
        }
      });
    } catch (err){
      logger.error('Error in getLogin.controller ' );
		  let errResponse = errHandler.errorResponse(errorcodes.get("ERRORCODE.ERRCD_301"), errorcodes.get("ERRMSG.ERRMSG_203_TEST_LOGIN"),'login', 'login');
        res.status(400).send(errResponse);
        console.log("errResponse::", errResponse);
        return res.status(400).end();
      }
};

async function getUSerDetails(requestData, res) {
  var query = `SELECT * FROM USERS WHERE USERID = ${requestData.userid}`;
  dbUtil.executequery(query, function(err, r) {
      if(err) {
          let errorMessage = errHandler.errorResponse(errorcodes.get("ERRORCODE.ERRCD_301"), errorcodes.get("ERRMSG.ERRMSG_203_TEST_LOGIN"),'test', 'login');
          return res.status(500).json(errorMessage).end(); 
      } else {
          if(requestData.password === r.rows[0].password){
            var jwtObj = {
              user_id: requestData.userid,
              role_id: r.rows[0].roleid
            };
            requestData.jwtObj = jwtObj;
            commonService.generateTokens(requestData, "LOGIN", function(err, response) {
              if(err) {
                return res.status(200).send({errors: [],result: {message: 'token could not generate!',title: 'User details!'}});
              } else {
                return res.status(200).send({
                  errors: [],
                  result: {message: 'User details fetched successfully!',title: 'User details!',
                    data: {
                          "userid":r.rows[0].userid,
                          "firstname":r.rows[0].firstname,
                          "lastname":r.rows[0].lastname,
                          "tkn": response.tkn
                    }
                  }
                });
              }
            });
          } else {
            return res.status(200).send({errors: [],result: {message: 'Invalid password!',title: 'Login password!'}});
          }
      }
  });
}





