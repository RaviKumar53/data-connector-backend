const moment = require('moment');

function isValidDateString(dateString){//YYYY-MM-DD Only!!!
	if(typeof dateString != 'string' || !dateString.length) return false
	var regEx = /^\d{4}-\d{2}-\d{2}$/;
	if(!dateString.match(regEx)) return false;  // Invalid format
	var d = new Date(dateString);
	if(!d.getTime()) return false; // Invalid date (or this could be epoch)
	return d.toISOString().slice(0, 10) === dateString;
}

exports.isValidDateString = isValidDateString;
