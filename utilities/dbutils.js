var pg = require('pg');
const fs = require('fs');
const loggerService = require('../services/logger_service.js');
const logger = new loggerService('app');

var config = require('../config/environment');
var envurl = config.envProperties;

module.exports = {
  executequery: executequery
};

console.time('TimeToDBconnection');

logger.info('db_host from local ::::', envurl.get('DB.db_host'));
logger.info('db_port from local ::::', envurl.get('DB.db_port'));

const { Pool } = require('pg')
const pool = new Pool({
  max: 20,
  idleTimeoutMillis: 30000,
  connectionTimeoutMillis: 2000,
  host: envurl.get('DB.db_host'),
  user: envurl.get('DB.db_user'),
  password: envurl.get('DB.db_password'),
  database: envurl.get('DB.db_database'),
  multipleStatements: true,
  debug: false
});

pool.connect(function(err, connection) {
  if (!err){
    console.timeEnd('TimeToDBconnection');
    logger.info('DB.db_host from process.env......', envurl.get('DB.db_host'));
    logger.info('Connection established to DB .........!!');
    connection.release();
  } else {
    console.timeEnd('TimeToDBconnection');
    logger.info('Failed to connect to Database ::::', JSON.stringify(err));
  }
});


function executequery(InputQuery, callback) {
  pool.connect(function(err, connection) {
    if (err) {
      logger.error('DB Disconnected...........!!' + JSON.stringify(err));
      callback(err);
    }
    console.log('DB Connected.........!!');
    connection.query(InputQuery, function(error, result) {
      try {
        connection.release();
        logger.info("Input Query: " + InputQuery);
        if (error) {
          logger.error('new error ::::', error);
          callback(error, null);
        }
        if (result) {
          callback(null, result);
        }
      } catch (e){
        logger.error('error is :::' + e + ':::' + InputQuery);
        console.log('error is :::' + e + ':::' + InputQuery);
      }
    });
  });
}