// Development specific configuration
// ==================================
module.exports = {
  logLevel: 'debug',
  BASIC_AUTH: {
    NM: 'xxxxx',
    PWD: 'xxxxx'
  },
  JWT: {
    SC_KEY: 'YhysSfDUWBlBAlZCIPlS',
    KEY_LFTM: (24 * 60 * 60), // 3600,
    ALG: 'HS256',
    REFRESH_SECRET: 'lAZNl8H5Lual4AY34G',
    REFRESH_LIFE: (30 * 24 * 60 * 60) // (60 * 60),
  }
};
