/**
 * Express configuration
 */

'use strict';

var express = require('express');
var morgan = require('morgan');
var compression = require('compression');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var errorHandler = require('errorhandler');
var path = require('path');
var config = require('./environment');

//export default function(app) {
exports.default = function(app) {
  var env = app.get('env');

  app.use(compression());
  app.use(bodyParser.urlencoded({ extended: false }));
  app.use(bodyParser.json());
  app.use(methodOverride());

  // Setting up HTTPS and redirecting HTTP calls to HTTPS
  app.enable('trust proxy');

  if ('production' === env) {
      app.use (function (req, res, next) {
          if (req.secure) {
              // Request was HTTPS, no additional handling required
              next();
          } else {
              // request was via HTTP, redirect to HTTPS
              res.redirect('https://' + req.headers.host + req.url);
          }
      });
  }

  app.set('appPath', path.join(config.root, 'client'));

  if ('production' === env) {
    app.use(express.static(app.get('appPath')));
    app.use(morgan('dev'));
  }

  if ('development' === env || 'test' === env) {
    app.use(express.static(path.join(config.root, '.tmp')));
    app.use(express.static(app.get('appPath')));
    app.use(morgan('combined'));
    app.use(errorHandler()); // Error handler - has to be last
  }
}
