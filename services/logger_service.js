const winston = require('winston');
const S3StreamLogger = require('s3-streamlogger').S3StreamLogger;

const s3_stream = new S3StreamLogger({
	bucket: "data-connector-log",
	access_key_id: "",
	secret_access_key: ""
});
dateFormat = () => {
	return new Date(Date.now()).toUTCString();
}
class LoggerService {
	constructor(route) {
		this.log_data = null
		this.route = route
		//this.route = './logs/application'
		const logger = winston.createLogger({
			format: winston.format.json(),
			transports: [
				new winston.transports.Console(),
				new winston.transports.File({
					filename: `./logs/application.log`   //${route} = ./logs/application
				}),
				// new winston.transports.Stream({
				// 	stream: s3_stream
				// })
			],
			format: winston.format.printf((info) => {
				let message = `${dateFormat()} | ${info.level.toUpperCase()} | ./logs/application.log | ${info.message} | `
				message = info.obj ? message + `data:${JSON.stringify(info.obj)} | ` : message
				message = this.log_data ? message + `log_data:${JSON.stringify(this.log_data)} | ` : message
				return message
			})
		});
		this.logger = logger;
	}
	setLogData(log_data) {
		this.log_data = log_data;
	}
	async info(message) {
		this.logger.log('info', message);
	}
	async info(message, obj) {
		this.logger.log('info', message, {
			obj
		})
	}
	async debug(message) {
		this.logger.log('debug', message);
	}
	async debug(message, obj) {
		this.logger.log('debug', message, {
			obj
		})
	}
	async error(message) {
		this.logger.log('error', message);
	}
	async error(message, obj) {
		this.logger.log('error', message, {
			obj
		})
	}
}
module.exports = LoggerService